<?php

$debug = FALSE;

$templates = array(
    'app/templates',
);

$app->site = $app->request->getRootUri();
$app->static = $app->site . '/public/static';
$app->media = $app->site . '/public/media';

require 'localsettings.php';