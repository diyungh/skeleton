<?php

$app->get('/', function() use ($app) {
    $app->render('skeleton/base.html');
});
$app->get('/hello/:name', function($name) use ($app) {
    $app->render('skeleton/hello.html', array(
        'name' => $name));
});